from flask import Flask
from application import config
from flask_jwt_extended import JWTManager
from flask_cors import CORS
from flask_mail import Mail
from flasgger import Swagger
from flask_socketio import SocketIO

socketio = SocketIO(cors_allowed_origins="*", async_mode="gevent")

def create_app():
    """Construct the core application."""
    app = Flask(__name__, instance_relative_config=False)
    enviroment = app.config["ENV"]
    # Application Configuration
    if enviroment == "production":
        app.config.from_object(config.ProductionConfig)
    if enviroment == "development":
        app.config.from_object(config.DevelopmentConfig)
    if enviroment == "test":
        app.config.from_object(config.TestConfig)
        app.config.from_object('config.Config')
    app.config["SWAGGER"] = {"title": "Swagger-UI", "uiversion": 3}
    app.config['JWT_AUTH_URL_RULE'] = '/api/auth'
    app.config['JWT_AUTH_HEADER_NAME'] = 'JWTAuthorization'
    with app.app_context():
        # Import parts of our application
         from .users.views import (sign_up_app, log_in_app, follow_app,
                                    user_app, token_app)
         from .posts.views import post_app, like_app, post_feed_app
         from .chat.views import chat_app, chat_historical_app
         app.register_blueprint(log_in_app, url_prefix='/api/v1')
         app.register_blueprint(sign_up_app, url_prefix='/api/v1')
         app.register_blueprint(post_app, url_prefix='/api/v1')
         app.register_blueprint(follow_app, url_prefix='/api/v1')
         app.register_blueprint(like_app, url_prefix='/api/v1')
         app.register_blueprint(post_feed_app, url_prefix='/api/v1')
         app.register_blueprint(chat_app, url_prefix='')
         app.register_blueprint(chat_historical_app, url_prefix='/api/v1')
         app.register_blueprint(user_app, url_prefix='/api/v1')
         app.register_blueprint(token_app, url_prefix='/api/v1')

    swagger_config = {
        "headers": [],
        "specs": [
            {
                "endpoint": "apispec_1",
                "route": "/apispec_1.json",
                "rule_filter": lambda rule: True,  # all in
                "model_filter": lambda tag: True,  # all in
            }
        ],
        "static_url_path": "/flasgger_static",
        # "static_folder": "static",  # must be set by user
        "swagger_ui": True,
        "specs_route": "/swagger/",
    }
    
    CORS(app)
    Mail(app)
    Swagger(app, config=swagger_config)
    JWTManager(app)
    socketio.init_app(app)
    return app