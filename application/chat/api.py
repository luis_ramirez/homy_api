from flask.views import MethodView
from flask import request, abort, json, jsonify, render_template
from flask_jwt_extended import jwt_required, get_jwt_identity
from flasgger.utils import swag_from
from .chat import (get_chat_historical)
class ChatAPI(MethodView):
    def get(self):
        return render_template('chat.html')

class ChatHistoricalAPI(MethodView):
    def __init__(self):
        if request.method != 'GET':
            abort(400)

    @jwt_required
    @swag_from("swagger/chat_historical.yaml")
    def get(self):
        try:
            username = get_jwt_identity()["username"]
        except:
            return {"error": "Token inválido"}, 400
        # username who your goning to send massages
        username_to = request.args.get("user_to")
        print(username_to)
        page = int(request.args.get("username_to", 0))
        try:
            result, status_code = get_chat_historical(username, username_to, page)
        except:
            return {"error": "Servicio no disponible por el momento"}, 500
        return jsonify(result), status_code