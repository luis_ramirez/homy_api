var socket = io()
document.addEventListener('DOMContentLoaded', () => {
    socket = socket.connect('http://' + document.domain + ':' + location.port);
    // only will work when you get the username from the session in the front
    socket.on('connect', data => {
        socket.emit('init_connection', {'username': localStorage.getItem('username')})
    })

    document.querySelector("#send-message").onclick = () => {
        socket.emit("user_message", {"message": document.querySelector("#user-message").value,
        "username": document.querySelector("#username").value,
        "recipient": document.querySelector("#username-to").value});
    };

    socket.on('user_response', data => {
        const p = document.createElement('p');
        const span_username = document.createElement('span');
        const br = document.createElement('br');
        span_username.innerHTML = 'from: ' + data.username_from;
        p.innerHTML = span_username.outerHTML + br.outerHTML + data.message + br.outerHTML;
        document.querySelector('.display-message-section').append(p)
    });
});

window.addEventListener('beforeunload', function (e) {
    e.preventDefault();
    socket.emit('remove_connection', {'username': localStorage.getItem('username')});
    e.returnValue = ''; 
});

window.addEventListener('onunload', function (e) {
    e.preventDefault();
    socket.emit('remove_connection', {'username': localStorage.getItem('username')});
    e.returnValue = ''; 
});