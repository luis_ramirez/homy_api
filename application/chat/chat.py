from .aggregations import get_chat_historical_pagination
from . import messages_historical_collection
def get_chat_historical(username: str, username_to: str, page: int):
    """
    username: is the conversation owner, and who makes the request
    username_to: is the one whose conversation is
    """
    aggregation = get_chat_historical_pagination(username, username_to, page)
    
    try:
        result = messages_historical_collection.aggregate(aggregation)
    except:
        return {"error": "Servicio no disponible"}, 500
    return list(result), 200