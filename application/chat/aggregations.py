def get_chat_historical_pagination(username, username_to, skip=0, amount=50):
    print([username, username_to])
    return [
        {
            '$match': {
                'username':  [
                    username, username_to
                ]
            }
        }, {
            '$unwind': {
                'path': '$queued_messages', 
                'preserveNullAndEmptyArrays': False
            }
        }, {
            '$sort': {
                'queued_messages.time': -1
            }
        }, {
            '$facet': {
                'metadata': [
                    {
                        '$count': 'total'
                    }
                ], 
                'data': [
                    {
                        '$project': {
                            'username': 0, 
                            '_id': 0
                        }
                    }, {
                        '$skip': skip
                    }, {
                        '$limit': amount
                    }
                ]
            }
        }
    ]