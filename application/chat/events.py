from .. import socketio
from flask import request
from flask_socketio import send, emit
from . import messages_historical_collection, redis_client
from datetime import datetime

@socketio.on('init_connection')
def handle_connection(data):
    print(data)
    redis_client.set(data["username"], request.sid)

@socketio.on('user_message')
def handle_my_custom_event(json):
    message_to = json['recipient']
    message_from = json['username']
    message = json['message']
    try:
        # search for the online users, if its not in dict it's offline
        message_to_id = redis_client.get(message_to).decode("utf-8")
        emit("user_response", {"message": message, "username_from": message_from}, room=message_to_id)
        add_message_to_historical(message, message_from, message_to)
    except:
        # add bd the users-to messages to been added when connects again
        add_message_to_historical(message, message_from, message_to)

def add_message_to_historical(message, user_from, user_to):
    print('agrego un mensaje')
    sorted_users = sorted([user_to, user_from])
    messages_historical_collection.update(
        {'username': sorted_users},
        {'$push': {
            "queued_messages": {
                    'message': message,
                    'from': user_from,
                    'time': datetime.now()
                }
            }
        }, 
        upsert=True
    )

@socketio.on('remove_connection')
def remove_connection(data):
    try:
        redis_client.delete(data['username'])
    except:
        pass