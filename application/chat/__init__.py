from pymongo import MongoClient
from flask import current_app as app
import redis

client = MongoClient(app.config["MONGO_URI"], ssl=True)
db = client.get_database(app.config["DB_NAME"])
posts_collection = db[app.config["POSTS_COLLECTION"]]
bucket_images_posts = app.config["BUCKET_POST_IMAGES"]
users_collection = db[app.config["USERS_COLLECTION"]]
messages_historical_collection = db[app.config["MESSAGES_HISTORICAL_COLLECTION"]]

redis_client = redis.Redis(
    host = app.config["REDIS_URI"],
    port = app.config["REDIS_PORT"],
    password = app.config["REDIS_PASSWORD"]
)

from . import events