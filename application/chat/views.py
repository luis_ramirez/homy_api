from flask import Blueprint
from .api import ChatAPI, ChatHistoricalAPI

chat_app = Blueprint("chat_app", __name__, template_folder='templates', static_folder='static', static_url_path='/chat_app/static')
chat_view = ChatAPI.as_view("chat_view")

chat_app.add_url_rule("/chat",
                    view_func=chat_view,
                    methods=["POST", "GET"])

chat_historical_app = Blueprint("chat_historical_app", __name__)
chat_historical_view = ChatHistoricalAPI.as_view("chat_historical_view")

chat_historical_app.add_url_rule("/chat_historical",
                    view_func=chat_historical_view,
                    methods=["GET"])