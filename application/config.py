import os
import pymongo
from pymongo import MongoClient
from dotenv import load_dotenv
import datetime

class Config():
    APP_NAME = "Homy Sell"
    MONGO_URI = os.getenv("MONGO_URI")
    PROFILE_IMAGES_BUCKET = os.getenv("PROFILE_IMAGES_BUCKET")
    POSTS_IMAGES_BUCKET = os.getenv("POSTS_IMAGES_BUCKET")
    DB_NAME = os.getenv("DB_NAME")
    USERS_COLLECTION = os.getenv("USERS_COLLECTION")
    POSTS_COLLECTION = os.getenv("POSTS_COLLECTION")
    JWT_SECRET_KEY = os.getenv("JWT_SECRET_KEY")
    MAX_CONTENT_LENGTH = 25 * 1024 * 1024
    BUCKET_POST_IMAGES = os.getenv("BUCKET_POST_IMAGES")
    SWAGGER = {"title": "Swagger-UI", "universion": 3}
    MESSAGES_HISTORICAL_COLLECTION = os.getenv("MESSAGES_HISTORICAL_COLLECTION")
    REDIS_URI = os.getenv("REDIS_URI")
    REDIS_PORT = os.getenv("REDIS_PORT")
    REDIS_PASSWORD = os.getenv("REDIS_PASSWORD")
    
class ProductionConfig(Config):
    DEBUG = False

class DevelopmentConfig(Config):
    DEBUG = True

class TestConfig(Config):
    TESTING = True