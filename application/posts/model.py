from . import posts_collection
from datetime import datetime
from bson.objectid import ObjectId

class Post:
    def __init__(self, **data):
        self.__dict__ = data
        self.date = datetime.now()
        self.likes = []

    def __repr__(self):
        return str(self.__dict__)

    def add_images(self, urls: list):
        self.image_urls = urls

    def __save__(self):
        posts_collection.insert(self.__dict__)

    def __update__(self, case_id):
        posts_collection.update({"_id": ObjectId(case_id)}, {"$set": self.__dict__})

    def add_username(self, username):
        self.username = username

    def check_username(self, username, post_id):
        try:
            current_username = list(posts_collection.find({"_id": ObjectId(post_id)}))[0]['username']
            print("acrtual {} sesión {} a cambiar {}".format(current_username, username, self.username))
            if username == current_username and username == self.username:
                return True
            return False
        except ValueError as err:
            return False
    
    def set_post_type(self, type):
        self.post_type = type

    def check_coordinates(self):
        # Disable all the no-member violations in this function
        # pylint: disable=no-member
        if not self.x_coord and not self.y_coord:
            return True
        if (self.x_coord and not self.y_coord) or (not self.x_coord and self.y_coord):
            return False
        if self.x_coord and self.y_coord:
            self.coodinates = [self.x_coord, self.y_coord]
            return True