from flask.views import MethodView
from flask import request, abort, json, jsonify
from .policies import (Postchema, TextPostchema,
    UpdatePostchema)
import marshmallow, json
from flasgger.utils import swag_from
from .posts import (upload_post_with_image, upload_post_without_image,
    like_post, get_user_feed, get_post_detail, delete_post,
    update_post_without_image)
from flask_jwt_extended import jwt_required, get_jwt_identity
from bson.json_util import dumps

class PostAPI(MethodView):
    @jwt_required
    @swag_from("swagger/upload_post.yaml")
    def post(self):
        """
            HTTP method: POST
            Handles the upload of posts with or without images
        """
        post_type = request.args.get("type")
        images = request.files.getlist("images")
        # checks the post's type to select the proper schema
        if post_type == "social":
            schema = TextPostchema()
        elif post_type == "residence":
            schema = Postchema()
        else:
            return {"error": "Tipo de post no válido"}, 400
        # creates the model with the given schema
        try: 
            post = schema.load(request.form.to_dict())
        except marshmallow.exceptions.ValidationError as validation: 
            return validation.messages, 400
        # Checks if the request contains images
        try:
            username = get_jwt_identity()["username"]
        except:
            return {"error": "Token inválido"}, 400
        if images:
            result, status_code = upload_post_with_image(post, images, post_type, username)
        else:
            result, status_code = upload_post_without_image(post, post_type, username)
        return jsonify(json.loads(dumps(result))), status_code

    @jwt_required
    @swag_from("swagger/post_detail.yaml")
    def get(self):
        """
            HTTP method: GET
            Gets the post detail by the correspondant id
        """
        post_id = request.args.get("id")
        if not post_id:
            return {"error": "Debes especificar una publicación"}, 405
        result, status_code = get_post_detail(post_id)
        result = json.loads(dumps(result))
        return jsonify(result), status_code

    @jwt_required
    @swag_from("swagger/post_delete.yaml")
    def delete(self):
        """"
            HTTP method: DELETE
            Delets certain post by it's ID.
            The user in session has to be the owner.
        """
        post_id = request.args.get("id")
        try:
            username = get_jwt_identity()["username"]
        except:
            return {"error": "Token inválido"}, 400
        if not post_id:
            return {"error": "Debes especificar una publicación"}, 405
        result, status_code = delete_post(post_id, username)
        return jsonify(result), status_code

    @jwt_required
    @swag_from("swagger/post_update.yaml")
    def patch(self):
        """"
            HTTP method: PATCH
            Updates certain post by it's ID.
            The user in session has to be the owner.
        """
        post_id = request.args.get("id")
        requested_post = request.form.to_dict()
        post_type = requested_post["post_type"]
        images = request.files.getlist("images")
        # checks the post's type to select the proper schema
        if post_type == "social":
            schema = TextPostchema()
        elif post_type == "residence":
            schema = UpdatePostchema()
        else:
            return {"error": "Tipo de post no válido"}, 400
        # creates the model with the given schema
        try: 
            post = schema.load(requested_post)
        except marshmallow.exceptions.ValidationError as validation: 
            return validation.messages, 400
        # Checks if the request contains images
        try:
            username = get_jwt_identity()["username"]
        except:
            return {"error": "Token inválido"}, 400
        if images:
            result, status_code = upload_post_with_image(post, images, post_type, username)
        else:
            result, status_code = update_post_without_image(post, post_id, username)
        return jsonify(result), status_code

class LikePostAPI(MethodView):
    def __init__(self):
        if request.method != 'POST':
            abort(400)

    @swag_from("swagger/like.yaml")
    def post(self):
        """
            HTTP method: POST
            Handles the likes to certain post
        """
        post_id = request.json.get("post_id")
        username = request.json.get("username")
        if post_id and username:
            result, status_code = like_post(post_id,  username)
        else: return {"error": "Deebes especificar el usuario y el post correctamente"}
        return jsonify(result), status_code

class PostFeedAPI(MethodView):
    def __init__(self):
        if request.method != 'GET':
            abort(400)

    @jwt_required
    @swag_from("swagger/post_feed.yaml")
    def get(self):
        """
            HTTP method: GET
            Serch for the user requested and brings their current feed
        """
        feed_type = request.args.get("type")
        if not feed_type: return {"error": "Tipo de feed no especificado"}
        if not feed_type == "followed" and not feed_type == "public": return {"error": "Tipo de feed inválido"}
        page = request.args.get("page", "0")
        page = int(page)
        try:
            username = get_jwt_identity()["username"]
        except:
            return {"error": "Token inválido"}, 400
        result, status_code = get_user_feed(username, feed_type, page)
        try:
            values = list(result)[0]
        except:
            values = []

        return jsonify(json.loads(dumps(values))), status_code