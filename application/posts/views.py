from flask import Blueprint
from .api import PostAPI, LikePostAPI, PostFeedAPI

post_app = Blueprint("post_app", __name__)
post_view = PostAPI.as_view("post_view")

post_app.add_url_rule('/post',
                    view_func=post_view,
                    methods=["POST", "GET", "DELETE", "PATCH"])

like_app = Blueprint("like_app", __name__)
like_view = LikePostAPI.as_view("like_view")

like_app.add_url_rule('/post/like',
                    view_func=like_view,
                    methods=["POST"])

post_feed_app = Blueprint("post_feed_app", __name__)
post_feed_view = PostFeedAPI.as_view("post_feed_view")

post_feed_app.add_url_rule('/feed',
                    view_func=post_feed_view,
                    methods=["GET", "PATCH"])