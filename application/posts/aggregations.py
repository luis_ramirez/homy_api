def post_feed_aggregation(type: str, page = 0, followed_list = []):
    if type == "followed":
        return [
            {
                '$match': {
                    'username': {"$in": followed_list}
                }
            }, {
                '$facet': {
                    'metadata': [
                        {
                            '$count': 'total'
                        }
                    ], 
                    'data': [
                        {
                            '$skip': page * 20
                        }, {
                            '$limit': 20
                        }, {
                            '$addFields': {
                                "likes_count": { '$size': "$likes" }
                            }
                        }, {
                            '$lookup': {
                                'from': 'users', 
                                'localField': 'username', 
                                'foreignField': 'username', 
                                'as': 'user'
                            }
                        }, {
                            '$project': {
                                'user.password': 0, 
                                'user._id': 0, 
                                'user.date': 0, 
                                'user.followed': 0, 
                                'user.followers': 0, 
                                'user.user_id': 0
                            }
                        }
                    ]
                }
            }
        ]
    else:
        if followed_list:
            return [
                {
                    '$match': {
                        'username': {"$nin": followed_list}
                    }
                }, {
                    '$facet': {
                        'metadata': [
                            {
                                '$count': 'total'
                            }
                        ], 
                        'data': [
                            {
                                '$skip': page * 20
                            }, {
                                '$limit': 20
                            }, {
                                '$sort': {
                                    'date': -1
                                }
                            }, {
                                '$addFields': {
                                    "likes_count": { '$size': "$likes" }
                                }
                            }, {
                                '$lookup': {
                                    'from': 'users', 
                                    'localField': 'username', 
                                    'foreignField': 'username', 
                                    'as': 'user'
                                }
                            }, {
                                '$project': {
                                    'user.password': 0, 
                                    'user._id': 0, 
                                    'user.date': 0, 
                                    'user.followed': 0, 
                                    'user.followers': 0, 
                                    'user.user_id': 0
                                }
                            }
                        ]
                    }
                }
            ]
        else:
            return [
                {
                    '$facet': {
                        'metadata': [
                            {
                                '$count': 'total'
                            }
                        ], 
                        'data': [
                            {
                                '$skip': page * 20
                            }, {
                                '$limit': 20
                            }, {
                                '$addFields': {
                                    "likes_count": { '$size': "$likes" }
                                }
                            }, {
                                '$lookup': {
                                    'from': 'users', 
                                    'localField': 'username', 
                                    'foreignField': 'username', 
                                    'as': 'user'
                                }
                            }, {
                                '$project': {
                                    'user.password': 0, 
                                    'user._id': 0, 
                                    'user.date': 0, 
                                    'user.followed': 0, 
                                    'user.followers': 0, 
                                    'user.user_id': 0
                                }
                            }
                        ]
                    }
                }
            ]