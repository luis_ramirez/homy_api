from .model import Post
from .helpers import allowed_image_extension
import random, string
from google.cloud import storage
from . import (bucket_images_posts, posts_collection, users_collection)
from bson.objectid import ObjectId
from .aggregations import post_feed_aggregation
from bson.objectid import ObjectId

def upload_post_with_image(post: Post, images: list, post_type: str, username: str):
    if post_type == "residence":
        try:
            if not post.check_coordinates():
                return {"error": "Debes indicar ambas coordenadas"}, 405
        except:
            pass
    for image in images:
        # Loop to check valid extension
        if not allowed_image_extension(image.filename):
            return {"error": "Extensión de imagen no válida"}, 422
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_images_posts)
    urls = []
    for image in images:
        # Loop to upload all images
        try:
            random_name = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))
            blob = bucket.blob(random_name)
            blob.upload_from_string(
                image.read(),
                content_type=image.content_type
            )
            urls.append(blob.public_url)
        except:
            return {"error": "Error al registrar el post, intente más tarde"}, 500
    post.add_images(urls)
    # adds the post type (social or residence)
    post.set_post_type(post_type)
    post.add_username(username)
    try:
        post.__save__()
    except:
        return {"error": "Caso ya existente"}, 500
    return {"message": "Se ha realizado tu publicación"}, 200

def upload_post_without_image(post: Post, post_type: str, username: str):
    if post_type == "residence":
        try:
            if not post.check_coordinates():
                return {"error": "Debes indicar ambas coordenadas"}, 405
        except:
            pass
    # adds the post type (social or residence)
    post.set_post_type(post_type)
    post.add_username(username)
    try:
        post.__save__()
        print(str(post.__repr__))
    except ValueError as err:
        print(err)
        return {"error": "Caso ya existente"}, 500
    return {"message": "Se ha realizado tu publicación", '_id': post._id}, 200

def like_post(post_id: str, username: str):
    """
        Give a like to a posot requeset
        Parameters:
            post_id: post's id
            username: username who  wnats give the like
    """
    try:
        posts_collection.update({"_id": ObjectId(post_id)}, {"$addToSet": {"likes": username}})
        return {"message": "Se ha dado like a la publicación"}, 200
    except:
        return {"error": "No se encuentra la publicación que buscas"}, 400

def get_user_feed(username: str, feed_type: str, page: int):
    """Get followed accounts by the user"""
    try:
        result = users_collection.find({"username": username}, {"_id": 0, "followed": 1})
        followed_list = result[0]["followed"]
    except:
        followed_list = []
    try:
        aggregation = post_feed_aggregation(feed_type, page, followed_list)
        result = posts_collection.aggregate(aggregation)
    except:
        return {"error": "Servicio no disponible"}, 500
    return result, 201

def get_post_detail(post_id: str):
    try:
        result = posts_collection.find({'_id': ObjectId(post_id)})
        result = list(result)[0]
    except:
        return [], 201
    
    try:
        result['likes_count'] = len(result['likes'])
    except:
        result['likes_count'] = 0
        
    return result, 200

def delete_post(post_id: str, username: str):
    try:
        result = posts_collection.find({'_id': ObjectId(post_id)})
        result = list(result)[0]
        if result["username"] == username:
            posts_collection.remove({'_id': ObjectId(post_id)})
            return {"message": "Se ha la publicación"}, 200
        else:
            return {"error": "La publicación no existe o no tienes permiso"}, 405
    except:
        return {"error": "Problema con la publicación, intenta más tarde"}, 500

def update_post_without_image(post: Post, post_id: str, username: str):
    try:
        if post.post_type == "residence":
            if not post.check_coordinates():
                return {"error": "Debes indicar ambas coordenadas"}, 405
    except ValueError as err:
        print(err)
        return {'error': 'Error en los datos de la publicación'}, 405
    try:
        if not post.check_username(username, post_id):
            return {'error': 'No tienes acceso a la publicación'}, 403
    except ValueError as err:
        return {"error": "Servicio no disponible"}, 500
    try:
        post.__update__(post_id)
    except:
        return {"error": "Caso ya existente"}, 405
    return {"message": "Se ha actualizado tu publicación tu publicación"}, 200