def allowed_image_extension(filename: str) -> bool:
    """Validates image name to containt a name and valid extension"""
    if not "." in filename: return False
    file_extension = filename.rsplit(".", 1)[1]
    if not file_extension.lower() in ["jpg", "jpeg", "png", "gif"]: return False
    return True