from marshmallow import (
    Schema,
    fields,
    validate,
    pre_load,
    post_load,
)
from .model import Post

class Postchema(Schema):
    title = fields.Str(required=True, validate=[validate.Length(min=3, error="El título debe tener una extesión mínima de 3 caracteres")])
    direction = fields.Str(required=True, validate=[validate.Length(min=3, error="La direccción debe tener al menos 3 caracteres")])
    direction2 = fields.Str(validate=[validate.Length(min=3, error="La direccción debe tener al menos 3 caracteres")])
    city = fields.Str(required=True, validate=[validate.Length(min=1, error="Debes especificar una ciudad")])
    state = fields.Str(required=True, validate=[validate.Length(min=1, error="Debes especificar un estado")])
    zip_code = fields.Str(required=True, validate=[validate.Length(min=1, error="Debes especificar un código postal")])
    residence_mode = fields.Str(required=True, validate=validate.OneOf(["venta", "renta"], error="Valor para el modo de residencia no válido"))
    text = fields.Str(validate=[validate.Length(min=2, error="El texto debe tener al menos 2 caracteres")])
    x_coord = fields.Float()
    y_coord = fields.Float()
    sharing = fields.Boolean(required=True)
    bedrooms = fields.Int()
    bathrooms = fields.Int()
    area = fields.Int()
    
    @post_load
    def create_user(self, data, **kwargs):
        return Post(**data)

class UpdatePostchema(Schema):
    title = fields.Str(required=True, validate=[validate.Length(min=3, error="El título debe tener una extesión mínima de 3 caracteres")])
    username = fields.Str(required=True, validate=[validate.Length(min=3, error="El nombre de usuario debe tener una extesión mínima de 2 caracteres")])
    post_type = fields.Str(required=True, validate=validate.OneOf(["residence", "social"], error="La publicación debe de ser de tipo residence o social"))
    direction = fields.Str(required=True, validate=[validate.Length(min=3, error="La direccción debe tener al menos 3 caracteres")])
    direction2 = fields.Str(validate=[validate.Length(min=3, error="La direccción debe tener al menos 3 caracteres")])
    city = fields.Str(required=True, validate=[validate.Length(min=1, error="Debes especificar una ciudad")])
    state = fields.Str(required=True, validate=[validate.Length(min=1, error="Debes especificar un estado")])
    zip_code = fields.Str(required=True, validate=[validate.Length(min=1, error="Debes especificar un código postal")])
    residence_mode = fields.Str(required=True, validate=validate.OneOf(["venta", "renta"], error="Valor para el modo de residencia no válido"))
    text = fields.Str(validate=[validate.Length(min=2, error="El texto debe tener al menos 2 caracteres")])
    x_coord = fields.Float()
    y_coord = fields.Float()
    #sharing = True#fields.Boolean(required=True)
    bedrooms = fields.Int()
    bathrooms = fields.Int()
    area = fields.Int()
    image_urls = fields.List(fields.Url())
    
    @post_load
    def create_user(self, data, **kwargs):
        return Post(**data)

class TextPostchema(Schema):
    title = fields.Str(validate=[validate.Length(min=2, error="El título debe tener una extesión mínima de 2 caracteres")])
    text = fields.Str(validate=[validate.Length(min=2, error="El texto debe tener al menos 2 caracteres")])

    @post_load
    def create_user(self, data, **kwargs):
        return Post(**data)
