from marshmallow import (
    Schema,
    fields,
    validate,
    pre_load,
    post_load,
)
from .model import User, Account

class UserSchema(Schema):
    name = fields.Str(required=True, validate=[validate.Length(min=3, error="El nombre debe tener una extesión mínima  caracteres")])
    last_name = fields.Str(required=True, validate=[validate.Length(min=3, error="El apellido debe tener una extesión mínima  caracteres")])
    username = fields.Str(required=True, validate=[validate.Length(min=3, error="El nombre de usuario debe tener una extesión mínima de 3 caracteres")])
    email = fields.Str(required=True, validate=[validate.Email(error="Formato de correo no válido")])
    organization = fields.Str()
    # pylint: disable=no-member
    password = fields.Str(required=True, 
        validate=validate.Regexp("^[a-zA-Z\d@$!#%*?&-._)(¿+><]{6,}$", # pylint: disable=W1401
        error="La contraseña debe tener al  menos 6 caractéres, puede incluir mayúsculas, minúsculas, caracteres especial y números"))
    
    @post_load
    def create_user(self, data, **kwargs):
        return User(**data)

class AccountSchema(Schema):
    email = fields.Str(required=True, validate=[validate.Email(error="Formato de correo no válido")])

    password = fields.Str(required=True, 
        validate=validate.Regexp("^[a-zA-Z\d@$!#%*?&-._)(¿+><]{6,}$", # pylint: disable=W1401
        error="La contraseña debe tener al  menos 6 caractéres, una mayúscula, una minúscula, un caracter especial y un número"))

    @post_load
    def create_account(self, data, **kwargs):
        return Account(**data)

class UpdateUserSchema(Schema):
    name = fields.Str(required=True, validate=[validate.Length(min=3, error="El nombre debe tener una extesión mínima  caracteres")])
    last_name = fields.Str(required=True, validate=[validate.Length(min=3, error="El apellido debe tener una extesión mínima  caracteres")])
    username = fields.Str(required=True, validate=[validate.Length(min=3, error="El nombre de usuario debe tener una extesión mínima de 3 caracteres")])
    email = fields.Str(required=True, validate=[validate.Email(error="Formato de correo no válido")])
    organization = fields.Str()

    @post_load
    def create_user(self, data, **kwargs):
        return User(**data)