import hashlib, binascii, os
from . import users_collection, posts_collection, chat_historical_collection
from .aggregations import get_max_user_id
from datetime import datetime
class User:
    def __init__(self, **data):
        self.__dict__ = data
        self.date = datetime.now()

    def __repr__(self):
        return self.__dict__

    def email_exists(self):
        return list(users_collection.find({"email": self.email}))
    
    def username_exists(self):
        return list(users_collection.find({"username": self.username}))

    def hash_password(self):
        salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
        pwdhash = hashlib.pbkdf2_hmac('sha512', self.password.encode('utf-8'), salt, 100000)
        pwdhash = binascii.hexlify(pwdhash)
        self.password = (salt + pwdhash).decode('ascii')

    def __save__(self):
        users_collection.insert(self.__dict__)
        
    def __update__(self):
        users_collection.update({"username": self.username}, {"$set": self.__dict__})

    def add_user_id(self):
        try:
            consecutivo = list(users_collection.aggregate(get_max_user_id))[0].get("user_id")
            self.user_id = consecutivo + 1
        except:
            self.user_id = 1

    def add_image(self, image):
        self.image = image

    def complete_update(self, session_username):
        # Change all post to match the new username
        posts_collection.update({'username': session_username}, {"$set": {"username": self.username}})
        # Updates chat historicals where the rquested username is included
        chat_historical_collection.update_many(
            {
                "queued_messages.from": session_username,
            }, 
            {
                "$set": {"queued_messages.$[].from": self.username}
            }
        )
        # Change the username in every historical and sorts the list for the right functionality
        for doc in chat_historical_collection.find({"username": session_username}):
            aux = map(lambda x: self.username if x == session_username else x, doc['username'])
            aux = sorted(aux)
            chat_historical_collection.update({"username": doc["username"]}, {"$set": {"username": aux}})
        users_collection.update({"username": session_username}, {"$set": {"username": self.username}})

class Account:
    def __init__(self, **data):
        self.__dict__ = data

    def __repr__(self):
        return self.__dict__

    def get_user_data(self):
        return users_collection.find_one({"email": self.email})