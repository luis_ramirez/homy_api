from flask.views import MethodView
from flask import request, abort, json, jsonify
from .model import User
from .policies import UserSchema, AccountSchema, UpdateUserSchema
from .users import (sign_up, log_in, follow_account,
    get_user_detail, update_user_info, update_user_info_image,
    refresh_token, get_followers)
import marshmallow, re
from flasgger.utils import swag_from
from flask_jwt_extended import (jwt_required, get_jwt_identity,
    jwt_refresh_token_required)
from bson.json_util import dumps

class SignUpAPI(MethodView):
    def __init__(self):
        if request.method != 'POST':
            abort(400)

    @swag_from("swagger/user_sign_up.yaml")
    def post(self):
        schema = UserSchema()
        try: 
            user = schema.load(request.json)
        except marshmallow.exceptions.ValidationError as validation:
            return validation.messages, 400
        result, status_code = sign_up(user)
        return jsonify(result), status_code

class LogInAPI(MethodView):
    def __init__(self):
        if request.method != 'POST':
            abort(400)
    @swag_from("swagger/user_log_in.yaml")
    def post(self):
        schema = AccountSchema()
        try: 
            account = schema.load(request.json)
        except marshmallow.exceptions.ValidationError as validation: 
            return validation.messages, 400
        result, status_code = log_in(account)
        return result, status_code

class FollowAPI(MethodView):
    @jwt_required
    @swag_from("swagger/get_follows.yaml")
    def get(self):
        try:
            username = get_jwt_identity()["username"]
        except:
            return {"error": "Token inválido"}, 400
        result, status_code = get_followers(username)
        return jsonify(result), status_code

    @swag_from("swagger/follow.yaml")
    def post(self):
        user_from = request.json.get("user_from")
        user_to = request.json.get("user_to")
        if user_from and user_to:
            result, status_code = follow_account(user_from, user_to)
        else: return {"error": "Debes especificar, ambos usuarios"}, 405
        return jsonify(result), status_code

class UserAPI(MethodView):
    @jwt_required
    @swag_from("swagger/user_detail.yaml")
    def get(self):
        try:
            username = get_jwt_identity()["username"]
        except:
            return {"error": "Token inválido"}, 400
        user = request.args.get('user')
        if user:
            result, status_code = get_user_detail(user)
        else:
            result, status_code = get_user_detail(username)
        return jsonify(json.loads(dumps(result))), status_code

    @jwt_required
    @swag_from("swagger/user_update.yaml")
    def patch(self):
        """
            HTTP method: PATCH
            Handles the update of user update, adds the profile image
        """
        image = request.files.get("image")
        session_username = get_jwt_identity()["username"]
        schema = UpdateUserSchema()
        try: 
            user = schema.load(request.form.to_dict())
        except marshmallow.exceptions.ValidationError as validation:
            return validation.messages, 400
        if image:
            result, status_code = update_user_info_image(user, session_username, image)
        else:    
            result, status_code = update_user_info(user, session_username)
        return jsonify(result), status_code

class TokenAPI(MethodView):
    @jwt_refresh_token_required
    @swag_from("swagger/refresh_token.yaml")
    def get(self):
        current_user = get_jwt_identity()
        result, status_code = refresh_token(current_user)
        return jsonify(result), status_code