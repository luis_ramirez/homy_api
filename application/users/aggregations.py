get_max_user_id = [
    {
        '$project': {
            'user_id': 1, 
            '_id': 0
        }
    }, {
        '$sort': {
            'user_id': -1
        }
    }, {
        '$limit': 1
    }
]

get_cases = [
    {
        '$project': {
            'topic': 1, 
            'sub_topic': 1
        }
    }
]

def user_info_aggregation(user_id: str):
    return [
        {
            '$match': {
                'username': user_id
            }
        }, {
            '$lookup': {
                'from': 'posts', 
                'localField': 'username', 
                'foreignField': 'username', 
                'as': 'posts'
            }
        }, {
            '$project': { 
                'date': 0, 
                'password': 0, 
                'user_id': 0,
                '_id': 0
            }
        }
    ]

def follows_aggregation(usernames: list):
    return [
        {
            '$match': {
                'username': {
                    '$in': usernames
                }
            }
        }, {
            '$project': {
                'username': 1, 
                'email': 1, 
                'image': 1, 
                'name': 1, 
                'last_name': 1,
                '_id': 0
            }
        }
    ]