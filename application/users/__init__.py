from pymongo import MongoClient
from flask import current_app as app

client = MongoClient(app.config["MONGO_URI"], ssl=True)
db = client.get_database(app.config["DB_NAME"])
users_collection = db[app.config["USERS_COLLECTION"]]
posts_collection = db[app.config["POSTS_COLLECTION"]]
chat_historical_collection = db[app.config["MESSAGES_HISTORICAL_COLLECTION"]]
bucket_images_posts = app.config["BUCKET_POST_IMAGES"]