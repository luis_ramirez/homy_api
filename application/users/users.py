from .helpers import validate_password
from .model import User, Account
from flask_jwt_extended import (create_access_token, create_refresh_token)
from . import users_collection
from flask import jsonify
from .aggregations import user_info_aggregation, follows_aggregation
from google.cloud import storage
from . import (bucket_images_posts, posts_collection, users_collection)
import random, string

def sign_up(user: User):
    try: 
        if user.email_exists(): return {"error": "Usuario ya existente"}, 400
        if user.username_exists(): return {"error": "Nombre de usuario ya existente"}, 400
        user.hash_password()
        user.add_user_id()
        user.__save__()
        return {"message": "Usuario registrado exitosamente"}, 200
    except:
        return {"error": "Error al registrar el usuario, intente más tarde"}, 500

def log_in(account: Account):
    try:
        user_data = account.get_user_data()
        if user_data:
            if not validate_password(user_data, account.password): return {"error": "Contraseña incorrecta"}, 400
        else: return {"error": "Usuario no existente"}, 400
    except:
        return {"error": "Servicio no disponible por el momento, intenta más tarde"}, 500
    login_result = {
        "mail": account.email,
        "user_id": user_data["user_id"],
        "username": user_data["username"],
    }
    access_token = create_access_token(login_result)
    refresh_token = create_refresh_token(login_result)
    login_result.update({"token": access_token, "refresh_token": refresh_token})
    login_result.update({
        "name": user_data["name"],
        "last_name": user_data["last_name"],
        "username": user_data["username"]
    })
    try:
        login_result["image"] = user_data["image"]
    except:
        login_result["image"] = ""
    resp = jsonify(login_result)
    resp.status_code = 200
    return resp, 200

def refresh_token(current_user):
    try:
        ret = {
            'token': create_access_token(identity=current_user)
        }
    except:
        return {'error', 'Servicio no disponible por el momento'}
    return ret, 200

def follow_account(user_from: str, user_to: str):
    """
        Follow an account, register in the user's followed array wich makes the request
        the user_to account. In the user_to follower array, register the user_from
        parameters. If the user is already followed it's need to remove em: 
            user_from -> username of the user making the request to follow
            user_to -> username of the followed user
    """
    if user_from == user_to:
        return {"error": "No puedes seguir tu misma cuenta"}, 401
    try:
        # Checks if the user is al ready following
        result_followed = users_collection.find(
            {
                "username": user_from,
                "followed": {"$in": [user_to]}
            }
        ).count()
        result_follower = users_collection.find(
            {
                "username": user_to,
                "followers": {"$in": [user_from]}
            }
        ).count()
        if result_followed == 0:
            # if is the user is not already followed then add to the followed list
            users_collection.update({"username": user_from}, {"$addToSet": {"followed": user_to}})
        else:
            # if is the user is already followed then add to the followed list
            users_collection.update({"username": user_from}, {"$pull": {"followed": user_to}})
        if result_follower == 0:
            # if is the user is not already a follower then add to the followed list
            users_collection.update({"username": user_to}, {"$addToSet": {"followers": user_from}})
        else:
            # if is the user is already a follower then add to the followed list
            users_collection.update({"username": user_to}, {"$pull": {"followers": user_from}})
        if result_followed == 0:
            return {"message": "Has comenzado a seguir a: {}".format(user_to)}, 200
        else:
            return {"message": "Has dejado de seguir a: {}".format(user_to)}, 200
    except:
        return {"error": "Servicio no disponible, intenta más tarde"}, 500

def get_user_detail(user_id: str):
    '''
        Gets the account information and posts of the user_id
        parameters:
            user_id -> user id that wants to get the information
    '''
    try:
        result = users_collection.aggregate(user_info_aggregation(user_id))
        user_info = list(result)
        if not user_info:
            return {'error': 'Usuario no encontrado'}, 405
        return user_info[0], 200
    except:
        return {'error': 'Servicio no disponible por el momento'}, 500

def update_user_info(user: User, session_username: str):
    '''
        Checks if the requested username exists, checks wich update
        case needs to be procesed:
            Cases:
                - The username remains the same
                - The username is looking for an update, this is
                the key reference for the actions in the app.
            user -> user object to be updated
    '''
    try:
        if session_username == user.username:
            user.__update__()
        else:
            user.complete_update(session_username)
        return user.__repr__(), 200
    except:
        return {'error': 'Hay un problema al actualizar tu perfil, intenta más tarde'}, 500
    return {'message': 'El se ha actualizado el usuario corréctamente'}, 200

def update_user_info_image(user: User, session_username: str, image):
    '''
        Checks if the requested username exists, checks wich update
        case needs to be procesed:
            Cases:
                - The username remains the same
                - The username is looking for an update, this is
                the key reference for the actions in the app.
            user -> user object to be updated
        When the request has a profile image is need to save it in the bucket first
        the save their val
    '''
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_images_posts)
    random_name = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))
    blob = bucket.blob(random_name)
    blob.upload_from_string(
        image.read(),
        content_type=image.content_type
    )
    user.add_image(blob.public_url)
    try:
        if session_username == user.username:
            user.__update__()
        else:
            user.complete_update(session_username)
        return user.__repr__(), 200
    except:
        return {'error': 'Hay un problema al actualizar tu perfil, intenta más tarde'}, 500
    return {'message': 'El se ha actualizado el usuario corréctamente'}, 200

def get_followers(username: str):
    try:
        user_result = list(users_collection.find({"username": username}))
    except:
        return {"error": "Problema con la petición, intenta más tarde"}, 500

    try:
        user_result = user_result[0]
    except:
        return {"message": "Usuario no existente"}, 201
    
    try:
        followers = user_result["followers"]
    except:
        followers = []

    try:
        followeds = user_result["followed"]
    except:
        followeds = []
    result = {
        "followers": list(
                users_collection.aggregate(follows_aggregation(followers))
            ),
        "followeds": list(
                users_collection.aggregate(follows_aggregation(followeds))
            )
    }
    return result, 200