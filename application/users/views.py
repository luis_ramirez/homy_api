from flask import Blueprint
from .api import (SignUpAPI, LogInAPI, FollowAPI,
    UserAPI, TokenAPI)

sign_up_app = Blueprint("sign_up_app", __name__)
sign_up_view = SignUpAPI.as_view("sign_up_view")

sign_up_app.add_url_rule('/user/sign_up',
                    view_func=sign_up_view,
                    methods=["POST"])

log_in_app = Blueprint("log_in_app", __name__)
log_in_view = LogInAPI.as_view("log_in_view")

sign_up_app.add_url_rule('/user/log_in',
                    view_func=log_in_view,
                    methods=["POST"])

follow_app = Blueprint("follow_app", __name__)
follow_view = FollowAPI.as_view("follow_view")

follow_app.add_url_rule('/user/follow',
                    view_func=follow_view,
                    methods=["GET", "POST"])

user_app = Blueprint("user_app", __name__)
user_view = UserAPI.as_view("user_view")

user_app.add_url_rule('/user',
                    view_func=user_view,
                    methods=["GET", "PATCH"])

token_app = Blueprint("token_app", __name__)
token_view = TokenAPI.as_view("token_view")

token_app.add_url_rule('/token/refresh',
                    view_func=token_view,
                    methods=["GET"])