import hashlib, binascii, os

def validate_password(password, account_password):
    """
    passwword: string stored in db
    account_password: string coming fron the user request
    """
    stored_password = password["password"]
    salt = stored_password[:64]
    stored_password = stored_password[64:]
    pwdhash = hashlib.pbkdf2_hmac('sha512', 
                            (account_password).encode('utf-8'), 
                            salt.encode('ascii'), 
                            100000)
    pwdhash = binascii.hexlify(pwdhash).decode('ascii')
    if pwdhash == stored_password:
        return True
    else: return False