from application import create_app, socketio
import marshmallow

app = create_app()

if __name__ == "__main__":
    print(app.config)
    socketio.run(app)